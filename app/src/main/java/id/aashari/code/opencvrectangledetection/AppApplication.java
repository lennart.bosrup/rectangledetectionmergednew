package id.aashari.code.opencvrectangledetection;

import android.app.Application;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppApplication extends Application {

        public static String XML_PLANK = "plank";
        public static String XML_LENGTH = "length";
        public static String XML_WIDTH = "width";
        public static String XML_DATE = "date";
        public static String XML_SORT = "sortering";
        public static String XML_ORDER = "order";
        public static String APP_FOLDER_NAME = "bluart";
        public static String CURRENT_ORDER_NAME = "currentOrderName";
        public static String DEFAULT_ORDER = "default";
        public static String SHARED_PREFS_CURRENT_ORDER_NAME = "currentOrderName";
        public static String SHARED_PREFS_LAST_ORDER_NAME = "lastOrderName";
    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
