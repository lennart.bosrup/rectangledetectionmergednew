package id.aashari.code.opencvrectangledetection;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.aashari.code.opencvrectangledetection.utils.AppLogger;
import id.aashari.code.opencvrectangledetection.utils.BleUtils;
import id.aashari.code.opencvrectangledetection.utils.DialogUtils;
import id.aashari.code.opencvrectangledetection.utils.Utils;

import static id.aashari.code.opencvrectangledetection.BluetoothLeService.ACTION_BLE_SCAN_RESULT;
import static id.aashari.code.opencvrectangledetection.BluetoothLeService.ACTION_GATT_CONNECTED;
import static id.aashari.code.opencvrectangledetection.BluetoothLeService.STATE_DISCONNECTED;

public class BluetoothBaseActivity extends AppCompatActivity {

    private boolean mScanning;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    AppLogger appLogger = new AppLogger(getClass().toString());
    BluetoothLeService.LocalBinder mBinder;
    private BluetoothScanResultAdapter bluetoothScanResultAdapter;
    private static final int NOTIFICATION_EX = 1;
    private NotificationManager notificationManager;
    private AlertDialog mRequestLocationDialog;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    // Activity request codes (used for onActivityResult)
    private static final int kActivityRequestCode_EnableBluetooth = 1;
    public static final int kActivityRequestCode_PlayServicesAvailability = 2;


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (BluetoothLeService.LocalBinder) service;
            appLogger.logInformation("On service connected");
            checkPermissions();
            bluetoothScanResultAdapter.setMBinder(mBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBinder = null;
            appLogger.logInformation("On service disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_device);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bluetoothScanResultAdapter = new BluetoothScanResultAdapter(getApplicationContext(), mBinder);
        recyclerView.setAdapter(bluetoothScanResultAdapter);
//        getActionBar().setTitle(R.string.title_devices);
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }


        notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
//                TODO show explanation

                requestStoragePermission();
            } else {
                // No explanation needed; request the permission
                requestStoragePermission();
            }
        } else {
            // Permission has already been granted

            requestCoarseLocationPermissionIfNeeded();

        }

    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    private void bindService() {
        Intent serviceIntent = new Intent(this, BluetoothLeService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unBindService() {
        unbindService(serviceConnection);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(ACTION_BLE_SCAN_RESULT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(ACTION_GATT_CONNECTED));
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mBinder != null && mBinder.getService() != null) {
            BluetoothLeService bluetoothLeService = mBinder.getService();
            bluetoothLeService.stopScan();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        unBindService();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            if (action == null) {

                return;
            }
            switch (action) {
                case ACTION_GATT_CONNECTED:
                    Intent plankConfigActivityIntent = new Intent(BluetoothBaseActivity.this, MainActivity.class);
                    startActivity(plankConfigActivityIntent);
                    break;
                case ACTION_BLE_SCAN_RESULT:
                    if (mBinder.getService() != null) {
                        BluetoothLeService bluetoothLeService = mBinder.getService();
                        List<BluetoothDevice> deviceList = bluetoothLeService.getDeviceList();
                        appLogger.logInformation("devices size " + deviceList.size());
                        bluetoothScanResultAdapter.updateDeviceList(deviceList);
                    }
                    break;
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        appLogger.logInformation("onCreateOptionsMenu");
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_scan).setEnabled(!mScanning);
        menu.findItem(R.id.menu_scan).setVisible(!mScanning);
        appLogger.logInformation("onPrepareOptionsMenu");
        return true;
    }

    void startScanning() {
        if (mScanning) {
            appLogger.logInformation("scanning true 1");
            return;
        }
        BluetoothLeService bluetoothLeService = mBinder.getService();
        if (bluetoothLeService != null) {
            if (bluetoothLeService.getConnectionState() != STATE_DISCONNECTED) {
                appLogger.logInformation("not STATE_DISCONNECTED");
                appLogger.logInformation(String.valueOf(bluetoothLeService.getConnectionState()));
                return;
            }
            if (bluetoothLeService.initialize()) {
                if (bluetoothLeService.startScan()) {
                    mScanning = true;
                    bluetoothScanResultAdapter.clearList();
                    invalidateOptionsMenu();
                    Utils.showNotification(getApplicationContext(), notificationManager, NOTIFICATION_EX);
                    appLogger.logInformation("scanning true 2");
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        BluetoothLeService bluetoothLeService1 = mBinder.getService();
                        if (bluetoothLeService1 != null) {
                            if (bluetoothLeService1.initialize()) {
                                bluetoothLeService1.stopScan();
                                mScanning = false;
                                Utils.dismissNotification(notificationManager, NOTIFICATION_EX);
                                invalidateOptionsMenu();
                                appLogger.logInformation("scanning false");
                            }
                        }
                    }, SCAN_PERIOD);
                }

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // display a message when a button was pressed
        String message = "";
        if (item.getItemId() == R.id.menu_scan) {
            checkPermissions();
        }

        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean requestCoarseLocationPermissionIfNeeded() {
        boolean permissionGranted = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android Marshmallow Permission check 
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionGranted = false;
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                mRequestLocationDialog = builder.setTitle(R.string.bluetooth_locationpermission_title)
                        .setMessage(R.string.bluetooth_locationpermission_text)
                        .setPositiveButton(android.R.string.ok, null)
                        .setOnDismissListener(dialog -> requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION))
                        .show();
            }
        }
        return permissionGranted;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    appLogger.logInformation("Location permission granted");

                    checkPermissions();

                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.bluetooth_locationpermission_notavailable_title);
                    builder.setMessage(R.string.bluetooth_locationpermission_notavailable_text);
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(dialog -> {
                    });
                    builder.show();
                }
                break;
            }
            case REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    requestCoarseLocationPermissionIfNeeded();
                } else {

                    BluetoothBaseActivity.this.finish();
                }
                return;
            }
            default:
                break;
        }
    }

    private void checkPermissions() {

        final boolean areLocationServicesReadyForScanning = manageLocationServiceAvailabilityForScanning();
        if (!areLocationServicesReadyForScanning) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            mRequestLocationDialog = builder.setMessage(R.string.bluetooth_locationpermission_disabled_text)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            //DialogUtils.keepDialogOnOrientationChanges(mRequestLocationDialog);
        } else {
            if (mRequestLocationDialog != null) {
                mRequestLocationDialog.cancel();
                mRequestLocationDialog = null;
            }

            // Bluetooth state
            final boolean isBluetoothEnabled = manageBluetoothAvailability();

            if (isBluetoothEnabled) {
                // Request Bluetooth scanning permissions
                final boolean isLocationPermissionGranted = requestCoarseLocationPermissionIfNeeded();

                if (isLocationPermissionGranted) {
                    // All good. Start Scanning
                    startScanning();
                }
            }
        }
    }

    // region Permissions
    private boolean manageLocationServiceAvailabilityForScanning() {

        boolean areLocationServiceReady = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {        // Location services are only needed to be enabled from Android 6.0
            int locationMode = Settings.Secure.LOCATION_MODE_OFF;
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            areLocationServiceReady = locationMode != Settings.Secure.LOCATION_MODE_OFF;
        }

        return areLocationServiceReady;
    }

    private boolean manageBluetoothAvailability() {
        boolean isEnabled = true;

        // Check Bluetooth HW status
        int errorMessageId = 0;
        final int bleStatus = BleUtils.getBleStatus(getApplicationContext());
        switch (bleStatus) {
            case BleUtils.STATUS_BLE_NOT_AVAILABLE:
                errorMessageId = R.string.bluetooth_unsupported;
                isEnabled = false;
                break;
            case BleUtils.STATUS_BLUETOOTH_NOT_AVAILABLE: {
                errorMessageId = R.string.bluetooth_poweredoff;
                isEnabled = false;      // it was already off
                break;
            }
            case BleUtils.STATUS_BLUETOOTH_DISABLED: {
                isEnabled = false;      // it was already off
                // if no enabled, launch settings dialog to enable it (user should always be prompted before automatically enabling bluetooth)
                Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetoothIntent, kActivityRequestCode_EnableBluetooth);
                // execution will continue at onActivityResult()
                break;
            }
        }

        if (errorMessageId != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog dialog = builder.setMessage(errorMessageId)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            DialogUtils.keepDialogOnOrientationChanges(dialog);
        }

        return isEnabled;
    }
}


