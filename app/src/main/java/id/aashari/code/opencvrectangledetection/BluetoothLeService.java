package id.aashari.code.opencvrectangledetection;

/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.ParcelUuid;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import id.aashari.code.opencvrectangledetection.listener.BluetoothOperationListener;
import id.aashari.code.opencvrectangledetection.utils.AppLogger;

import static id.aashari.code.opencvrectangledetection.utils.Utils.convertFromInteger;
import static java.lang.Thread.sleep;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_DISCONNECTING = 3;

    public final static String ACTION_GATT_CONNECTED =
            "net.mavericklabs.blueart.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "net.mavericklabs.blueart.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "net.mavericklabs.blueart.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "net.mavericklabs.blueart.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "net.mavericklabs.blueart.EXTRA_DATA";
    public final static String ACTION_BLE_SCAN_RESULT =
            "net.mavericklabs.blueart.ble_scan_result";
    public final static UUID UART_SERVICE_UUID = convertFromInteger(0x4444);
    public final static UUID UART_CHAR_UUID = convertFromInteger(0x5555);
    private AppLogger appLogger = new AppLogger(getClass().getName());

    private boolean mScanning;
    private List<BluetoothDevice> devices;
    private BluetoothGattCharacteristic uartCharacteristic;
    private List<BluetoothOperationListener> listeners;
    private boolean isWriting;
    private String stringToWrite;
    private String masterString;

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            appLogger.logInformation("onReliableWriteCompleted");
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                appLogger.logInformation("Connected to GATT server.");
                // Attempts to discover services after successful connection.
                appLogger.logInformation("Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                appLogger.logInformation("Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            } else {
                mConnectionState = newState;
            }
            for (BluetoothOperationListener listener :
                    listeners) {
                listener.onConnectionStateChange(newState);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                BluetoothGattService uartService = gatt.getService(UART_SERVICE_UUID);
                uartCharacteristic = uartService.getCharacteristic(UART_CHAR_UUID);
                isWriting = false;
                masterString = null;
            } else {
                appLogger.logInformation("onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            String stringWritten = new String((characteristic.getValue()));
            appLogger.logInformation("Wrote characteristic " + stringWritten);
            boolean isSuccessful = false;
            if (stringToWrite.equals(stringWritten) && status == BluetoothGatt.GATT_SUCCESS) {
                isSuccessful = true;
            }
            count++;
            processNextWrite(isSuccessful);

        }

    };
    private int count;

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(BluetoothLeService.this).sendBroadcast(intent);
    }

    public boolean write(String value) {
        if (mConnectionState != STATE_CONNECTED) {
            return false;
        }
        if (!isWriting) {
            masterString = value;
            count = 0;
            processNextWrite(true);
            isWriting = true;
            return true;
        }
        return false;

    }

    private void processNextWrite(boolean isSuccessful) {

        int beginIndex;
        int endIndex;

        if (!isSuccessful){
            notifyListeners(false);
            isWriting = false;
            return;
        }

        // Check if done
        switch (count){
            case 0:
                beginIndex = 0;
                if (masterString.length() > 20){
                    endIndex = 20;
                }else{
                    endIndex = masterString.length();
                }
                break;
            case 1:
                if (masterString.length() <= 20){
                    notifyListeners(true);
                    isWriting = false;
                    return;
                }else{
                    beginIndex = 20;
                    endIndex = masterString.length();
                }
                break;
            default:
                notifyListeners(true);
                isWriting = false;
                return;
        }
        appLogger.logInformation("count " + count);
        appLogger.logInformation("beginIndex " + beginIndex);
        appLogger.logInformation("endIndex " + endIndex);
        stringToWrite = masterString.substring(beginIndex, endIndex);
        appLogger.logInformation("stringToWrite " + Arrays.toString(stringToWrite.getBytes()));
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        uartCharacteristic.setValue(stringToWrite);
        mBluetoothGatt.writeCharacteristic(uartCharacteristic);

    }

    private void notifyListeners(boolean isSuccessful) {
        for (BluetoothOperationListener listener :
                listeners) {
            listener.onCharacteristicWrite(isSuccessful);
        }
    }

    public void addListener(BluetoothOperationListener listener) {
        listeners.add(listener);
    }

    public void removeListener(BluetoothOperationListener listener) {
        listeners.remove(listener);
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        devices = new ArrayList<>();
        listeners = new ArrayList<>();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    public final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                appLogger.logError("Unable to initialize BluetoothManager.");
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            appLogger.logError("Unable to obtain a BluetoothAdapter.");
            return false;
        }
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean startScan() {
        if (mBluetoothAdapter == null) {
            appLogger.logInformation("BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        ScanSettings.Builder builderScanSettings = new ScanSettings.Builder();
        builderScanSettings.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        builderScanSettings.setReportDelay(0);
        List<ScanFilter> filters = getScanFilters();
        mBluetoothAdapter.getBluetoothLeScanner().startScan(mLeScanCallback);
//            mBluetoothAdapter.getBluetoothLeScanner().startScan(filters,builderScanSettings.build(),mLeScanCallback);
        mScanning = true;
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean stopScan() {
        if (mBluetoothAdapter == null) {
            appLogger.logInformation("BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        if (mScanning) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(mLeScanCallback);
        }
        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(BluetoothDevice device) {
        if (mBluetoothAdapter == null) {
            appLogger.logInformation("BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        if (mConnectionState == STATE_CONNECTING || mConnectionState == STATE_CONNECTED) {
            return false;
        }
        mBluetoothGatt = device.connectGatt(this, true, mGattCallback);
        appLogger.logDebug("Trying to create a new connection.");
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    // Device scan callback.
    private ScanCallback mLeScanCallback =
            new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    BluetoothDevice device = result.getDevice();
                    updateDeviceList(device);
                    Intent intent = new Intent(ACTION_BLE_SCAN_RESULT);
                    LocalBroadcastManager.getInstance(BluetoothLeService.this).sendBroadcast(intent);
                }

            };

    private void updateDeviceList(BluetoothDevice device) {
        boolean deviceFound = false;
        for (int i = 0; i < devices.size(); i++) {
            if (device.getAddress().equals(devices.get(i).getAddress())) {
                deviceFound = true;
            }
        }
        if (!deviceFound) {
            devices.add(device);
        }
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            appLogger.logInformation("BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    private List<ScanFilter> getScanFilters() {
        return Collections.singletonList(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(UART_SERVICE_UUID.toString()))
                        .build());
    }

    public List<BluetoothDevice> getDeviceList() {
        return devices;
    }


    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public int getConnectionState() {
        return mConnectionState;
    }
}