package id.aashari.code.opencvrectangledetection;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.aashari.code.opencvrectangledetection.utils.AppLogger;


public class BluetoothScanResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 1;
    private static final int VIEW_TYPE_DATA = 2;
    private Context applicationContext;
    private BluetoothLeService.LocalBinder mBinder;
    private List<BluetoothDevice> devices = new ArrayList<>();

    private AppLogger appLogger = new AppLogger(getClass().toString());

    public BluetoothScanResultAdapter(Context applicationContext, BluetoothLeService.LocalBinder mBinder) {
        this.applicationContext = applicationContext;
        this.mBinder = mBinder;
    }

    @Override
    public int getItemViewType(int position) {
        if (devices.size() == 0) {
            return VIEW_TYPE_EMPTY;
        } else {
            return VIEW_TYPE_DATA;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_TYPE_EMPTY) {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_event_empty, parent, false);
            vh = new EmptyViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.listitem_device, parent, false);
            vh = new BluetoothScanResultViewHolder(v);
        }
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position) {
        appLogger.logDebug(position + " position");

        if (devices.size() == 0) {
            return;
        }
        BluetoothScanResultViewHolder viewHolder1 = (BluetoothScanResultViewHolder) viewHolder;
        BluetoothDevice device = devices.get(position);
        viewHolder1.deviceAddress.setText(device.getAddress());
        viewHolder1.deviceName.setText(device.getName());
        viewHolder1.connectButton.setOnClickListener(v -> {
            appLogger.logInformation("pressed button");
            if (mBinder == null) {
                appLogger.logInformation("mbinder null");
                return;
            }
            BluetoothLeService bluetoothService = mBinder.getService();
            if (bluetoothService != null) {
                if (bluetoothService.connect(device)){
                    appLogger.logInformation("Called connect");

                }else{
                    Toast.makeText(applicationContext,"Bluetooth is busy or not enabled", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    @Override
    public int getItemCount() {
        if (devices.size() == 0) {
            return 1;
        } else {
            return devices.size();
        }
    }

    public void updateDeviceList(List<BluetoothDevice> updatedDevices) {
        devices = updatedDevices;
        notifyDataSetChanged();
    }

    public void clearList() {
        devices.clear();
        notifyDataSetChanged();
    }

    public void setMBinder(BluetoothLeService.LocalBinder mBinder) {
        this.mBinder = mBinder;
    }

    class BluetoothScanResultViewHolder extends RecyclerView.ViewHolder {
        private final Button connectButton;
        TextView deviceName, deviceAddress;

        BluetoothScanResultViewHolder(View view) {
            super(view);
            deviceAddress = view.findViewById(R.id.device_address);
            deviceName = view.findViewById(R.id.device_name);
            connectButton = view.findViewById(R.id.connect_button);
        }
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {
        EmptyViewHolder(View view) {
            super(view);
        }
    }

}