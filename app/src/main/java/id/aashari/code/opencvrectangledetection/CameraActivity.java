package id.aashari.code.opencvrectangledetection;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

public class CameraActivity extends AppCompatActivity {
    private static final int REQUEST_WRITE_PERMISSION = 1;
    public static final int TAKE_PHOTO_CODE = 1;
    Long lastlast = 0L;
    public String camera = Environment.getExternalStorageDirectory() + File.separator +"DCIM"+ File.separator +"Camera";
    public String plankpics = Environment.getExternalStorageDirectory() + File.separator +"Pictures"+ File.separator +"Planks"+ File.separator;
    public static String backfilename = Environment.getExternalStorageDirectory() + File.separator +"Pictures"+ File.separator +"Planks"+ File.separator + "backpic.jpg";
    public Intent fotointent;
    Imgcodecs imageCodex = new Imgcodecs();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Date date = new Date();
        //final SharedPreferences.Editor editor = sharedPref.edit();
        setContentView(R.layout.activity_camera);
        lastlast = date.getTime();
        //lastlast=sharedPref.getLong("last",lastlast);
        MainActivity.comingfromMain = false;
        fotointent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(fotointent, TAKE_PHOTO_CODE);
//        final DirectoryObserver kollaDir=new DirectoryObserver(camera,FileObserver.CREATE);
//        kollaDir.startWatching();


        final Handler myHandler = new Handler();
        final int delay = 300; // 1000 milliseconds == 1 second

        myHandler.postDelayed(new Runnable() {
            public void run() {
                File file=new File(camera);

                if(file.exists()){
                    File[] list = file.listFiles();
                    if (list.length>0) {
                        for (File f: list){
                            String name = f.getName();
                            Long lastmod=f.lastModified();
                            if(lastmod>lastlast) {
                                //                                lastlast=lastmod;
//                                //Do your shit here
//                                editor.putLong("last",lastlast);
//                                editor.apply();
//                                BitmapFactory.Options options = new BitmapFactory.Options();
//                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//                                if(MainActivity.takebackground){
//                                    File backfile = new File(backfilename);
//                                    f.renameTo(backfile);
//                                    //moveFile(camera,name, plankpics);
//                                    MainActivity.takebackground =false;
//                                    MainActivity.backimage = BitmapFactory.decodeFile(f.toString(), options);
//                                }
//                                else {
                                MainActivity.picFileName=f.toString();
                                MainActivity.frontMat=imageCodex.imread(MainActivity.picFileName);
                                //MainActivity.image = BitmapFactory.decodeFile(f.toString(), options);
                                moveFile(camera,name, plankpics);
                                MainActivity.backfromFoto=true;
                                MainActivity.comingfromMain=false;
                                //}
                                finish();
                                stopcamera();
                                return;
                            }
                        }
                        myHandler.postDelayed(this, delay);
                    }
                }    }
        }, delay);
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck== PackageManager.PERMISSION_GRANTED){
            //this means permission is granted and you can do read and write
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
            }

        }

    }

    private void moveFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath+ File.separator + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + File.separator +inputFile).delete();


        }

        catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }
    private void stopcamera() {
        finishActivity(TAKE_PHOTO_CODE);
    }

}
