package id.aashari.code.opencvrectangledetection;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import android.view.KeyEvent;
import android.view.inputmethod.BaseInputConnection;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static id.aashari.code.opencvrectangledetection.AppApplication.XML_DATE;
import static id.aashari.code.opencvrectangledetection.AppApplication.XML_LENGTH;
import static id.aashari.code.opencvrectangledetection.AppApplication.XML_ORDER;
import static id.aashari.code.opencvrectangledetection.AppApplication.XML_PLANK;
import static id.aashari.code.opencvrectangledetection.AppApplication.XML_SORT;
import static id.aashari.code.opencvrectangledetection.AppApplication.XML_WIDTH;
import static id.aashari.code.opencvrectangledetection.AppApplication.getDate;
import static java.lang.System.currentTimeMillis;

public class MainActivity extends AppCompatActivity {
    Button btnAng, btnStart, btnSkog, btnSlut;
    ImageView imageView; TextView textW, textL, textPlankfile;
    public static Document doc;
    public static final int RequestPermissionCode = 1;
    private static final int REQUEST_WRITE_PERMISSION = 1;
    public static final int TAKE_PHOTO_CODE = 1;
    public int pixW=0, pixL=0;
    public String sortering="Skog";
    public static ArrayList<Plank> planks;
    Long lastTimemillis =0L;
    // public static Bitmap image, backimage;
    Intent intent;
    public static Mat frontMat;
    public String plankDir = Environment.getExternalStorageDirectory() + File.separator +"ekplankXml"+ File.separator +"plank";

    public String xmlDir=plankDir + "1.xml";
    public String Dirtext="Spara i nr "; public int Sparindex=1;
    public String rectDir = Environment.getExternalStorageDirectory() + File.separator +"DCIM"+ File.separator +"Rect";
    public static String picFileName;
    public static Boolean backfromFoto=false, comingfromMain=true, takebackground =false;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        CvScalar cvScalar = new CvScalar();
//        static CvScalar min = cvScalar(0, 0, 130, 0);//BGR-A
//        static CvScalar max= cvScalar(140, 110, 255, 0);//BGR-A

        OpenCVLoader.initDebug();
        setContentView(R.layout.activity_main);
        frontMat=new Mat(4160,2080,CvType.CV_16UC4);
        planks=new ArrayList<Plank>();
        btnAng = findViewById(R.id.angbutton);
        btnStart = findViewById(R.id.startbutton);
        btnSkog=findViewById(R.id.skogbutton);
        btnSlut=findViewById(R.id.slutabutton);
        imageView = findViewById(R.id.imageView);
        textW=findViewById(R.id.pixW);
        textL=findViewById(R.id.pixL);
        textPlankfile=findViewById(R.id.xmlDir);
        textPlankfile.setText(Dirtext + Sparindex);
        xmlDir=Dirtext + Sparindex;
        EnableRuntimePermission();
        readXML(xmlDir);
 //       File file = new File(CameraActivity.backfilename);


        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck== PackageManager.PERMISSION_GRANTED){
            //this means permission is granted and you can do read and write
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
            }
        }
    }
    public void startcamera() {
        intent = new Intent( this, CameraActivity.class);
        startActivity(intent);
    }

    public int calcWidth(int pix) {
        ArrayList<Integer> widths = new ArrayList<>(Arrays.asList(90, 108, 135, 180, 224, 270, 314, 360, 400, 450, 495));
        for(int width : widths){
            if((int) pix*pixToMm(pix)<width+18)return width;
        }
        return 540;
    }
    public int calcLength(int pix) {
        return (int) (pix*pixToMm(pix));
    }
    public double pixToMm(int pix) {
        return (pix-953)*0.04967/1745 + 1.080797;
    }
    public void saveValues() {
        int L=calcLength(pixL);
        if(L>0) {
            planks.add(new Plank(L,calcWidth(pixW),getDate(currentTimeMillis(),"yy-MM-dd hh:mm"),sortering));
            try {
                saveToXML(xmlDir);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        }
    }
    public void readXML(String filePath) {
        //First read file if exist

        try {
            File file = new File(filePath);
            InputStream is = new FileInputStream(file);
            if (is == null) return;

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();
            NodeList nodeList = element.getElementsByTagName(XML_PLANK);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element plankElement = (Element) nodeList.item(i);
                Node lengthNode = plankElement.getElementsByTagName(XML_LENGTH).item(0);
                Node widthNode = plankElement.getElementsByTagName(XML_WIDTH).item(0);
                Node dateNode = plankElement.getElementsByTagName(XML_DATE).item(0);
                Node sortNode = plankElement.getElementsByTagName(XML_SORT).item(0);
                String dateoTime = "", sortering = "";
                if (!(dateNode == null)) dateoTime = dateNode.getTextContent();
                if (!(sortNode == null)) sortering = sortNode.getTextContent();
                Plank plank = new Plank(lengthNode.getTextContent(), widthNode.getTextContent(), dateoTime, sortering);
                planks.add(plank);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    public static void saveToXML(String filePath) throws ParserConfigurationException {


        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();


            // root elements
            doc = docBuilder.newDocument();

            Element orderElement = doc.createElement(XML_ORDER);
            doc.appendChild(orderElement);

            for (Plank plank :
                    planks) {

                Element plankElement = doc.createElement(XML_PLANK);

                Element lengthElement = doc.createElement(XML_LENGTH);
                lengthElement.appendChild(doc.createTextNode(String.valueOf(plank.getLength())));
                plankElement.appendChild(lengthElement);

                Element widthElement = doc.createElement(XML_WIDTH);
                widthElement.appendChild(doc.createTextNode(String.valueOf(plank.getWidth())));
                plankElement.appendChild(widthElement);

                Element sortElement = doc.createElement(XML_SORT);
                sortElement.appendChild(doc.createTextNode(plank.getSort()));
                plankElement.appendChild(sortElement);

                Element dateElement = doc.createElement(XML_DATE);
                dateElement.appendChild(doc.createTextNode(plank.getDate()));
                plankElement.appendChild(dateElement);

                orderElement.appendChild(plankElement);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // write the content into xml file
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileOutputStream(filePath));

            transformer.transform(source, result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onBackPressed() {
        if (true) {
            super.onBackPressed();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onResume() {
        MainActivity.comingfromMain=true;
        super.onResume();
        if(backfromFoto) {

            soyouareback();
        }
    }

    public void soyouareback(){
        //code here to analyse image and ask user to continue
        pixL=0;pixW=0;
        imageView.setImageDrawable(null);

        imageCrop();
        textL.setText("Längd: " + String.valueOf(calcLength(pixL)));
        textW.setText("Bredd: " + String.valueOf(calcWidth(pixW)));
        if(pixL>0)saveValues();
    }

    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.CAMERA)) {
            Toast.makeText(MainActivity.this,"CAMERA permission allows us to Access CAMERA app",     Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);
        }
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(lastTimemillis>currentTimeMillis()-3500)return false;
        //alternativ med game-läge
//        KeyEvent.KEYCODE_BUTTON_X                         //uppe
        //        KeyEvent.KEYCODE_BUTTON_A                         //vänster
//        KeyEvent.KEYCODE_BUTTON_B                                 //nere
        //        KeyEvent.KEYCODE_BUTTON_Y                                 //höger
        lastTimemillis=currentTimeMillis();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:            //uppe
                //Äng
                sortering="Äng";
               // if(backfromFoto) saveValues();
                comingfromMain=true;
                startcamera();
                return super.dispatchKeyEvent(event);
            case KeyEvent.KEYCODE_VOLUME_DOWN:          //vänster
                sortering="Skog";
             //   if(backfromFoto) saveValues();
                comingfromMain=true;
                startcamera();
                return false;
            case KeyEvent.KEYCODE_ENTER:                //nere  --> Spara i nästa
                Sparindex++; if(Sparindex==5) Sparindex=1;
                textPlankfile.setText(Dirtext + Sparindex);
                xmlDir = plankDir + Sparindex +".xml";
                planks.clear();
                readXML(xmlDir);
                return false;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:   //stora knappen uppåt --> Spara i nästa
                Sparindex++; if(Sparindex==5) Sparindex=1;
                textPlankfile.setText(Dirtext + Sparindex);
                xmlDir = plankDir + Sparindex +".xml";
                planks.clear();
                readXML(xmlDir);
                return false;
            case KeyEvent.KEYCODE_MEDIA_REWIND:         //stora knappen neråt --> Spara i förra
                Sparindex--; if(Sparindex==0) Sparindex=4;
                textPlankfile.setText(Dirtext + Sparindex);
                xmlDir = plankDir + Sparindex +".xml";
                planks.clear();
                readXML(xmlDir);
                return false;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:     //lilla knappen i mitten

                return false;
            case KeyEvent.KEYCODE_BACK:                 //höger-->inte godkänd radera senaste
//                btnStart.setText("Starta --> Enter");
                KeyEvent.changeAction(event,0);
                return false;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
    public <exception> void imageCrop(){
        //avgänsa bilden på höjden
        Rect narrow=new Rect(850,0,400,4160);
        Random random = new Random();
        int j = random.nextInt(10000) ;
        //imageView.setImageBitmap(bitmap);
        Mat narrowMat = new Mat();
        Mat threshold= new Mat(4160,2080,CvType.CV_16UC4);
    try {
        narrowMat=frontMat.submat(narrow);

        Core.inRange(narrowMat,new Scalar(70,120,150,0),new Scalar(180,240,260,0),threshold);
    }
    catch (  Exception e) {
        return;
    }

//TODO:
        String filnamn = rectDir + "/threshold" +j + ".jpg";
        Imgcodecs.imwrite(filnamn, threshold);
        filnamn = rectDir + "/frontMat" +j + ".jpg";
        Imgcodecs.imwrite(filnamn, narrowMat);
        Mat imgSource= narrowMat.clone();
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(threshold, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_TC89_L1);

        Integer H, W;
        H=narrowMat.height(); W=narrowMat.width();
        Rect rec =new Rect();
        ;pixL=0;pixW=0;
        for(int i=0; i< contours.size();i++){
            if (Imgproc.contourArea(contours.get(i)) > 10000 )
            {
                Rect rect = Imgproc.boundingRect(contours.get(i));
                if (rect.height < H-5 && rect.width < W-5 )                 //mindre än hela
                    if(rect.height>800 && rect.width>88 && rect.x>0 && rect.y>0)  {       //det är en planka
                        pixW=rect.width;pixL=rect.height;
                        rec = new Rect(rect.x, rect.y, pixW, pixL);
                        break;
                    }
            }
        }
        if(rec.width>88) {
            Mat roi = imgSource.submat(rec);
            Bitmap bitmap1;
            bitmap1 =  Bitmap.createBitmap(rec.width, rec.height,Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(roi,bitmap1);
            imageView.setImageBitmap(bitmap1);
//TODO:  Insert sending a success-message
//    String filename = rectDir + "/Rect " +  j  + ".png";
//    Imgcodecs.imwrite(filename, roi);
        }
        else {
//TODO:    Insert a fail-message
        }

    }


    /*------------------------------------ Store Image -------------------------------------------*/
    public void saveTempImage(Bitmap bitmap) {
        if (isExternalStorageWritable()) {
            saveImage(bitmap);
        }else{
            Toast.makeText(this, "Please provide permission to write on the Storage!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/sams_images");

        if (! myDir.exists()){
            myDir.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }

        String  timeStamp = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
        String fname = timeStamp +".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    /*------------------------------------ ************* -----------------------------------------*/

    public void onCharacteristicWrite(boolean isSuccessful) {

    }

    public void onConnectionStateChange(int newState) {
        if (newState == BluetoothLeService.STATE_DISCONNECTED || newState == BluetoothLeService.STATE_DISCONNECTING) {
            MainActivity.this.runOnUiThread(this::showAlertDialogForBluetoothDisconnect);
        }
    }

    private void showAlertDialogForBluetoothDisconnect(){
        runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Bluetooth device disconnected. Taking to you bluetooth base activity");
            builder.setCancelable(false);
            builder.setPositiveButton(
                    "Okay",
                    (dialog, id) -> {
                        dialog.cancel();
                        Intent intent = new Intent(MainActivity.this, BluetoothBaseActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    });

            AlertDialog alert = builder.create();
            alert.show();
        });

    }
    /*------------------------------------ ************* -----------------------------------------*/


}