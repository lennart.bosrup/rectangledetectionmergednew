package id.aashari.code.opencvrectangledetection;

public class Plank {
    private int length;
    private String sortering="Skog";
    private int width;
    private String datumoTid;
    public Plank(int length, int width, String datumoTid, String sortering) {
        this.length = length;
        this.width = width;
        this.datumoTid = datumoTid;
        this.sortering=sortering;
    }

    public Plank(String length, String width, String datumoTid, String sortering) {
        this.length = Integer.valueOf(length);
        this.width = Integer.valueOf(width);
        this.datumoTid = datumoTid;
        this.sortering = sortering;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }
    public String getDate() {
        return datumoTid;
    }
    public String getSort() {
        return sortering;
    }
}
