package id.aashari.code.opencvrectangledetection.listener;

public interface BluetoothOperationListener {
    void onCharacteristicWrite(boolean isSuccessful);

    void onConnectionStateChange(int newState);
}
