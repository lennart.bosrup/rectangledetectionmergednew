package id.aashari.code.opencvrectangledetection.utils;

import android.util.Log;

public class AppLogger {
    private final String className;
    public AppLogger(String className) {
        this.className = className;
    }

    public void logDebug(String message){
        Log.d(this.className,"DEBUG : "+message);
    }
    public void logError(String message){
        Log.e(this.className,"ERR : "+message);
    }
    public void logWarning(String message){
        Log.w(this.className,"WARN : "+message);
    }
    public void logInformation(String message){
        Log.i(this.className, "INFO : " + message);
    }
}
