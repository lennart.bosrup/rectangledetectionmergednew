package id.aashari.code.opencvrectangledetection.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class AppSharedPreferences {

    private static String name = "Prefs";
    /**
     * currentOrder - String, if non then.
     */

        public static <T extends Long> void saveToPreferences(Context context, String preferenceName, T preferenceValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(preferenceName, (Long) preferenceValue);
            editor.apply();
        }

        public static <T extends String> void saveToPreferences(Context context, String preferenceName, T preferenceValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(preferenceName, (String) preferenceValue);
            editor.apply();
        }

        public static <T extends Boolean> void saveToPreferences(Context context, String preferenceName, T preferenceValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(preferenceName, (Boolean) preferenceValue);
            editor.apply();
        }

        public static <T extends String> String readFromPreferences(Context context, String preferenceName, T defaultValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getString(preferenceName, defaultValue);
        }

        public static <T extends Long> Long readFromPreferences(Context context, String preferenceName, T defaultValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getLong(preferenceName, (Long) defaultValue);
        }

        public static <T extends Boolean> Boolean readFromPreferences(Context context, String preferenceName, T defaultValue) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getBoolean(preferenceName, (Boolean) defaultValue);
        }

        public static boolean contains(Context context, String key) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.contains(key);
        }

        public static void removeFromPreferences(Context context, String preferenceName) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(preferenceName);
            editor.apply();
        }

}
