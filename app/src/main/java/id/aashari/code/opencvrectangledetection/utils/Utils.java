package id.aashari.code.opencvrectangledetection.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import id.aashari.code.opencvrectangledetection.R;


public class Utils {
    private static AppLogger appLogger = new AppLogger("Utils");

    public static Element xmlParse(String path) {
        try {
            InputStream is = openFileStream(path);
            if (is == null) {
                appLogger.logError("Error while reading file");
                return null;
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);

            Element element = doc.getDocumentElement();
            element.normalize();
            return element;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private static String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    private static FileInputStream openFileStream(String path) {

        File file = new File(path);
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }

    public static void showNotification(Context applicationContext, NotificationManager notificationManager, int notificationEx){

        int icon = R.drawable.baseline_bluetooth_searching_white_24;

        CharSequence contentTitle = applicationContext.getResources().getString(R.string.app_name);
        CharSequence contentText = "Scanning for bluetooth devices.";
        Notification.Builder builder = new Notification.Builder(applicationContext);
        builder.setSmallIcon(icon);
        builder.setOngoing(true);
        builder.setContentText(contentText);
        builder.setContentTitle(contentTitle);
        Notification notification = builder.build();

        notificationManager.notify(notificationEx, notification);
    }

    public static void dismissNotification(NotificationManager notificationManager, int notificationEx){
        notificationManager.cancel(notificationEx);
    }

}

